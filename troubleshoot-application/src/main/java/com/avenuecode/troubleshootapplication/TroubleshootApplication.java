package com.avenuecode.troubleshootapplication;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@SpringBootApplication
@RestController
public class TroubleshootApplication {

	Logger logger= LoggerFactory.getLogger(TroubleshootApplication.class);

	@GetMapping("/getUser/{id}")
	public Customer getCustomerById(@PathVariable int id) {
		List<Customer> customerList = getAllCustomers();
		Customer customer = customerList.stream().
				filter(u->u.getId()==id).findAny().orElse(null);
		if(customer!=null){
			logger.info("Customer found : {}", customer);
			return customer;
		}else{
			try {
				throw new Exception();
			} catch (Exception e) {
				e.printStackTrace();
				logger.error("Customer Not Found with ID : {}", id);
			}
			return new Customer();
		}
	}

	private List<Customer> getAllCustomers() {
		return Stream.of(new Customer(1, "Stephanie", "4th street", 27),
						new Customer(2, "James P. Sullivan", "5th street", 28),
						new Customer(3, "Mike Wazowski", "6th street", 29),
						new Customer(4, "Randall Woss", "7th street", 30))
				.collect(Collectors.toList());
	}

	public static void main(String[] args) {
		SpringApplication.run(TroubleshootApplication.class, args);
	}
}
